#
# docker build -t csabasulyok/python-pandoc:staging .
#

FROM ubuntu:22.04

# install tools
RUN apt update && \
        apt install -yqq pandoc python3-dev python3 python3-pip xzdec python3-pygments && \
        apt -y autoremove && rm -rf /var/lib/apt/lists

ENV PYTHONIOENCODING=UTF-8

# make python command alias to python3
RUN ln -s -T /usr/bin/python3 /usr/bin/python

WORKDIR /workspace
